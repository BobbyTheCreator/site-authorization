<?php
include_once 'config.php';

function autoload_classes($class)
{
    if (file_exists('./models/'.$class.'.php')) {
        require_once './models/'.$class.'.php';
    }
}

function autoload_controllers($class)
{
    if (file_exists('./controllers/'.$class.'.php')) {
        require_once './controllers/'.$class.'.php';
    }
}

spl_autoload_register('autoload_classes');
spl_autoload_register('autoload_controllers');

$currentPage = explode('/', $_SERVER['REQUEST_URI'])[1];
if (empty($_SESSION['authorizedUser']) && 'login' !== $currentPage && 'register' !== $currentPage) {
    header('Location: '.'/login');
    exit;
}
if (!empty($_SESSION['authorizedUser']) && 'profile' !== $currentPage) {
    header('Location: '.'/profile');
    exit;
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My awesome site</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        <?php include_once 'web/js/form-validation.js'; ?>
    </script>
    <style>
        <?php include_once 'web/css/site.css'; ?>
        <?php include_once 'web/css/authorization.css'; ?>
        <?php include_once 'web/css/profile.css'; ?>
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav d-flex justify-content-between custom-navbar-container">
                <li class="nav-item">
                    <a class="nav-link" href="/profile"><?= $_SESSION['translations']['Profile'] ?></a>
                </li>
                <?php if (!empty($_SESSION['authorizedUser']) && 'login' !== $currentPage && 'register' !== $currentPage): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/actions/logout.php"><?= $_SESSION['translations']['Sign out'] ?></a>
                    </li>
                <?php endif; ?>
            </ul>
    </nav>
    <?php require_once('Routes.php'); ?>

    <div class="footer bg-dark">
        <a href="/<?= $currentPage ?>/?lang=en"><?= $_SESSION['translations']['English'] ?></a>
         |
        <a href="/<?= $currentPage ?>/?lang=ru"><?= $_SESSION['translations']['Russian'] ?></a>
    </div>
</body>
</html>