<?php
    session_start();

    if (!isset($_SESSION['lang'])) {
        $_SESSION['lang'] = 'en';
    } elseif (!empty($_GET['lang']) && $_SESSION['lang'] !== $_GET['lang']) {
        switch ($_GET['lang']) {
            case 'en':
                $_SESSION['lang'] = 'en';
                break;
            case 'ru':
                $_SESSION['lang'] = 'ru';
                break;
        }
    }

    /* @var array $lang */
    require_once 'translations/'.$_SESSION['lang'].'.php';
    $_SESSION['translations'] = $lang;
