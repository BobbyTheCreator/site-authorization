<?php

class Register extends Controller
{
    public static function renderView(): View
    {
        return (new View(['translations' => $_SESSION['translations']]));
    }
}
