<?php

class Login extends Controller
{
    public static function renderView(): View
    {
        return (new View(['translations' => $_SESSION['translations']]));
    }
}
