<?php

class Profile extends Controller
{
    public static function renderView(): View
    {
        $user = User::findOne('SELECT * FROM user WHERE email = :email', [':email' => $_SESSION['authorizedUser']]);

        return (new View(
            [
                'user' => $user,
            ]
        ));
    }
}
