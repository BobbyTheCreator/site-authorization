<?php

class Controller extends Database
{
    public static function CreateView(string $viewName): void
    {
        $view = static::renderView();
        require_once("views/$viewName.php");
    }
}
