<?php
/* @var View $view */
/* @var array $translations */
$translations = $view->params['translations'];
?>
<script>
    // Send settings from php into web/js/form-validation.js init(settings) function
    <?= 'formValidation.init('.json_encode(
    [
            'errorTranslations' => [
                'Username cannot be empty' => $translations['Username cannot be empty'],
                'Email cannot be empty' => $translations['Email cannot be empty'],
                'Invalid email' => $translations['Invalid email'].$translations['.. Ex: '].'example@gmail.com',
                'Phone number cannot be empty' => $translations['Phone number cannot be empty'],
                'Wrong number of digits' => $translations['Wrong number of digits'].$translations['. Ex: '].'9045418492',
                'Phone number has non digits' => $translations['Phone number has non digits'].$translations['. Ex: '].'9045418492',
                'File cannot be empty' => $translations['File cannot be empty'],
                'Invalid file format' => $translations['Invalid file format'].$translations['. Ex: '].'jpg, jpeg, png, gif',
                'Password cannot be empty' => $translations['Password cannot be empty'],
                'Passwords do not match' => $translations['Passwords do not match'],
                'Confirmation password cannot be empty' => $translations['Password cannot be empty'],
            ],
        ],
    JSON_THROW_ON_ERROR
).')'?>
</script>

<?php
$codeErrorString = filter_input(INPUT_GET, 'err');
$emailError = $passwordError = '';
if (!empty($codeErrorString)) {
    $numberOfErrors = strlen($codeErrorString);
    for ($i = 0; $i < $numberOfErrors; $i++) {
        switch ($codeErrorString[$i]) {
            case SignInError::EMAIL_IS_EMPTY:
                $emailError = $translations['Email cannot be empty'];
                break;
            case SignInError::INVALID_EMAIL:
                $emailError = $translations['Invalid email'].$translations['. Ex: '].'example@gmail.com';
                break;
            case SignInError::PASSWORD_IS_EMPTY:
                $passwordError = $translations['Password cannot be empty'];
                break;
            case SignInError::EMAIL_OR_PASSWORD_DO_NOT_MATCH:
                $passwordError = $translations['Email or password do not match'];
                break;
        }
    }
}
?>

<div class="form-container">
    <div class="row form-width">
        <div class="col-12 mx-auto">
            <div class="card card-signin flex-row my-5">
                <div class="card-body">
                    <h5 class="card-title text-center"><?= $translations['Login'] ?></h5>
                    <form class="form-signin" method="POST" action="../actions/login.php">
                        <div class="form-label-group">
                            <input type="text" id="inputEmail" name="inputEmail" placeholder="<?= $translations['Email address'] ?>"
                                   value="<?= Helper::fieldValueAfterFailedValidation('inputEmail') ?>"
                                   class="form-control js-email-validation <?= $emailError ? 'error' : '' ?>">
                            <label for="inputEmail"><?= $translations['Email address'] ?></label>
                            <small class="error-message"><?= $emailError ?></small>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="inputPassword" placeholder="<?= $translations['Password'] ?>"
                                   class="form-control js-password-validation <?= $passwordError ? 'error' : '' ?>">
                            <label for="inputPassword"><?= $translations['Password'] ?></label>
                            <small class="error-message"><?= $passwordError ?></small>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button class="btn btn-primary text-uppercase" type="submit"><?= $translations['Login'] ?></button>
                        </div>
                        <hr>
                        <div class="text-center secondary-text"><?= $translations['Don\'t have an account yet?'] ?></div>
                        <a class="d-block text-center mt-2 small" href="/register"><?= $translations['Sign up'] ?></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>