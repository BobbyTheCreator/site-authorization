<?php
/* @var View $view */
/* @var User $user */

$user = $view->params['user'];

?>

<div class="container profile-container">
    <div class="row">
        <div class="col-md-4">
            <div class="profile-img">
                <img src="<?= $user->getPhotoLink() ?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="profile-head">
                <h4><?= $user->getUsername() ?></h4>
                <h6 style="margin-bottom: 75px"><?= $_SESSION['translations']['My awesome status'] ?></h6>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#" role="tab"
                           aria-controls="home" aria-selected="true"><?= $_SESSION['translations']['Bio'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" role="tab"
                           aria-controls="other" aria-selected="false"><?= $_SESSION['translations']['Other'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#" role="tab"
                           aria-controls="tabs" aria-selected="false"><?= $_SESSION['translations']['Tabs'] ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2" style="text-align: end">
            <a href="#" class="profile-edit-btn"><?= $_SESSION['translations']['Edit'] ?></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="profile-work">
                <p class="text-uppercase"><?= $_SESSION['translations']['Portfolio'] ?></p>
                <a href="#">Website</a><br/>
                <a href="#">GitHub</a><br/>
                <p class="text-uppercase"><?= $_SESSION['translations']['Skills'] ?></p>
                <div>PHP</div>
                <div>Docker</div>
                <div>jQuery</div>
                <div>HTML</div>
                <div>CSS</div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="tab-content profile-tab">
                <div class="tab-pane show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <label><?= $_SESSION['translations']['Name'] ?></label>
                        </div>
                        <div class="col-md-6">
                            <p><?= $user->getUsername() ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Email</label>
                        </div>
                        <div class="col-md-6">
                            <p><?= $user->getEmail() ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><?= $_SESSION['translations']['Phone'] ?></label>
                        </div>
                        <div class="col-md-6">
                            <p><?= $user->getPhoneNumber() ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><?= $_SESSION['translations']['Profession'] ?></label>
                        </div>
                        <div class="col-md-6">
                            <p><?= $_SESSION['translations']['Backend Developer'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>