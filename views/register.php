<?php
/* @var View $view */
/* @var array $translations */
$translations = $view->params['translations'];
?>
<script>
    // Send settings from php into web/js/form-validation.js init(settings) function
    <?= 'formValidation.init('.json_encode(
    [
            'errorTranslations' => [
                'Username cannot be empty' => $translations['Username cannot be empty'],
                'Email cannot be empty' => $translations['Email cannot be empty'],
                'Invalid email' => $translations['Invalid email'].$translations['. Ex: '].'example@gmail.com',
                'Phone number cannot be empty' => $translations['Phone number cannot be empty'],
                'Wrong number of digits' => $translations['Wrong number of digits'].$translations['. Ex: '].'9045418492',
                'Phone number has non digits' => $translations['Phone number has non digits'].$translations['. Ex: '].'9045418492',
                'File cannot be empty' => $translations['File cannot be empty'],
                'Invalid file format' => $translations['Invalid file format'].$translations['. Ex: '].'jpg, png, gif',
                'Password cannot be empty' => $translations['Password cannot be empty'],
                'Passwords do not match' => $translations['Passwords do not match'],
                'Confirmation password cannot be empty' => $translations['Password cannot be empty'],
                'Email or password do not match' => $translations['Email or password do not match'],
            ],
        ],
    JSON_THROW_ON_ERROR
).')'?>
</script>

<?php
$codeErrorString = filter_input(INPUT_GET, 'err');
$userNameError = $emailError = $phoneNumberError = $fileError = $passwordError = $confirmPasswordError = '';
if (!empty($codeErrorString)) {
    $numberOfErrors = strlen($codeErrorString);
    for ($i = 0; $i < $numberOfErrors; $i++) {
        switch ($codeErrorString[$i]) {
            case SignUpError::USERNAME_IS_EMPTY:
                $userNameError = $translations['Username cannot be empty'];
                break;
            case SignUpError::EMAIL_IS_EMPTY:
                $emailError = $translations['Email cannot be empty'];
                break;
            case SignUpError::INVALID_EMAIL:
                $emailError = $translations['Invalid email'].$translations['. Ex: '].'example@gmail.com';
                break;
            case SignUpError::EMAIL_ALREADY_EXISTS:
                $emailError = $translations['Email already exists'];
                break;
            case SignUpError::PHONE_NUMBER_IS_EMPTY:
                $phoneNumberError = $translations['Phone number cannot be empty'];
                break;
            case SignUpError::WRONG_NUMBER_OF_DIGITS_FOR_PHONE_NUMBER:
                $phoneNumberError = $translations['Wrong number of digits'].$translations['. Ex: '].'9045418492';
                break;
            case SignUpError::PHONE_NUMBER_HAS_NON_DIGITS:
                $phoneNumberError = $translations['Phone number has non digits'].$translations['. Ex: '].'9045418492';
                break;
            case SignUpError::PHONE_NUMBER_ALREADY_EXISTS:
                $phoneNumberError = $translations['Phone number already exists'];
                break;
            case SignUpError::FILE_IS_EMPTY:
                $fileError = $translations['File cannot be empty'];
                break;
            case SignUpError::INVALID_FILE_FORMAT:
                $fileError = $translations['Invalid file format'].$translations['. Ex: '].'jpg, png, gif';
                break;
            case SignUpError::UPLOAD_FILE_TOO_LARGE:
                $fileError = $translations['Upload file is too large. Max size: 4 MB'];
                break;
            case SignUpError::PASSWORD_IS_EMPTY:
                $passwordError = $translations['Password cannot be empty'];
                break;
            case SignUpError::PASSWORDS_DO_NOT_MATCH:
                $confirmPasswordError = $translations['Passwords do not match'];
                break;
            case SignUpError::CONFIRM_PASSWORD_IS_EMPTY:
                $confirmPasswordError = $translations['Confirmation password cannot be empty'];
                break;
        }
    }
}
?>

<div class="form-container">
    <div class="row form-width">
        <div class="col-12 mx-auto">
            <div class="card-signin card flex-row my-5">
                <div class="card-body">
                    <h5 class="card-title text-center"><?= $translations['Register'] ?></h5>
                    <form class="form-signin" method="POST" action="../actions/register.php" enctype="multipart/form-data">
                        <div class="form-label-group">
                            <input type="text" id="inputUsername" name="inputUsername" placeholder="<?= $translations['Username'] ?>"
                                   value="<?= Helper::fieldValueAfterFailedValidation('inputUsername') ?>"
                                   class="form-control js-username-validation <?= $userNameError ? 'error' : '' ?>">
                            <label for="inputUsername"><?= $translations['Username'] ?></label>
                            <small class="error-message"><?= $userNameError ?></small>
                        </div>

                        <div class="form-label-group">
                            <input type="text" id="inputEmail" name="inputEmail" placeholder="<?= $translations['Email address'] ?>"
                                   value="<?= Helper::fieldValueAfterFailedValidation('inputEmail') ?>"
                                   class="form-control js-email-validation <?= $emailError ? 'error' : '' ?>">
                            <label for="inputEmail"><?= $translations['Email address'] ?></label>
                            <small class="error-message"><?= $emailError ?></small>
                        </div>

                        <div class="form-label-group">
                            <input type="text" id="inputPhoneNumber" name="inputPhoneNumber" placeholder="<?= $translations['Phone number'] ?>"
                                   value="<?= Helper::fieldValueAfterFailedValidation('inputPhoneNumber') ?>"
                                   class="form-control js-phone-number-validation <?= $phoneNumberError ? 'error' : '' ?>">
                            <label for="inputPhoneNumber"><?= $translations['Phone number'] ?></label>
                            <small class="error-message"><?= $phoneNumberError ?></small>
                        </div>

                        <label for="inputFile" class="mb-1" style="color: #495057;">
                            <?= $translations['Profile photo'] ?>
                        </label>
                        <input id="inputFile" name="inputFile" type="file" style="border-radius:16px"
                               class="form-control js-file-validation" <?= $fileError ? 'error' : '' ?>>
                        <small class="error-message"><?= $fileError ?></small>

                        <hr>

                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="inputPassword" placeholder="<?= $translations['Password'] ?>"
                                   class="form-control js-password-validation <?= $passwordError ? 'error' : '' ?>">
                            <label for="inputPassword"><?= $translations['Password'] ?></label>
                            <small class="error-message"><?= $passwordError ?></small>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="inputConfirmPassword" name="inputConfirmPassword" placeholder="<?= $translations['Confirm password'] ?>"
                                   class="form-control js-confirm-password-validation <?= $confirmPasswordError ? 'error' : '' ?>">
                            <label for="inputConfirmPassword"><?= $translations['Confirm password'] ?></label>
                            <small class="error-message"><?= $confirmPasswordError ?></small>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button class="btn btn-primary custom-button text-uppercase" type="submit"><?= $translations['Sign up'] ?></button>
                        </div>
                        <hr>
                        <a class="d-block text-center small" href="/login"><?= $translations['Sign in'] ?></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>