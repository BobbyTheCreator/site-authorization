<?php

declare(strict_types=1);

final class SignUpError
{
    public const USERNAME_IS_EMPTY = 1;
    public const EMAIL_IS_EMPTY = 2;
    public const INVALID_EMAIL = 3;
    public const EMAIL_ALREADY_EXISTS = 'b';
    public const FILE_IS_EMPTY = 4;
    public const INVALID_FILE_FORMAT = 5;
    public const UPLOAD_FILE_TOO_LARGE = 'f';
    public const PASSWORD_IS_EMPTY = 6;
    public const CONFIRM_PASSWORD_IS_EMPTY = 7;
    public const PASSWORDS_DO_NOT_MATCH = 8;
    public const PHONE_NUMBER_IS_EMPTY = 9;
    public const WRONG_NUMBER_OF_DIGITS_FOR_PHONE_NUMBER = 'd';
    public const PHONE_NUMBER_HAS_NON_DIGITS = 'a';
    public const PHONE_NUMBER_ALREADY_EXISTS = 'c';
}
