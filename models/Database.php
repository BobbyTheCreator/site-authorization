<?php

declare(strict_types=1);

class Database
{
    public static string $host = 'mysql';
    public static string $dbName = 'sait';
    public static string $username = 'root';
    public static string $password = 'root';
    public static string $port = '3306';


    private static function connect(): PDO
    {
        $pdo = new PDO("mysql:host=".self::$host.";port=".self::$port.";dbname=".self::$dbName.";charset=utf8", self::$username, self::$password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    /**
     * @return array|void
     */
    public static function executeQuery(string $query, array $params = [])
    {
        $statement = self::connect()->prepare($query);
        $statement->execute($params);

        if (explode(' ', $query)[0] === 'SELECT') {
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
    }
}
