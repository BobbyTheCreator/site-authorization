<?php

declare(strict_types=1);

final class SignInError
{
    public const EMAIL_IS_EMPTY = 1;
    public const INVALID_EMAIL = 2;
    public const PASSWORD_IS_EMPTY = 3;
    public const EMAIL_OR_PASSWORD_DO_NOT_MATCH = 4;
}
