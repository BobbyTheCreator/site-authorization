<?php

declare(strict_types = 1);

final class Helper
{
    public static function testInput(string $data): string
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);

        return $data;
    }

    public static function fieldValueAfterFailedValidation(string $field = ''): string
    {
        if (!empty($field) && isset($_SESSION['formFields']) && array_key_exists($field, $_SESSION['formFields']) && !empty($_SESSION['formFields'][$field])) {
            return $_SESSION['formFields'][$field];
        }

        return '';
    }
}
