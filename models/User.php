<?php

declare(strict_types=1);

require_once 'interfaces/IDatabase.php';
require_once 'Database.php';

final class User implements IDatabase
{
    private int $id;
    private string $username;
    private string $email;
    private string $phoneNumber;
    private string $photoLink;
    private string $passwordHash;
    private DateTimeImmutable $createdAt;
    private DateTimeImmutable $updatedAt;

    public function __construct(string $username, string $email, string $phoneNumber, string $photoLink, string $passwordHash)
    {
        $this->username = $username;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->photoLink = $photoLink;
        $this->passwordHash = $passwordHash;
    }

    public function insert(): void
    {
        Database::executeQuery(
            'INSERT INTO user (username, email, phone_number, photo_link, password_hash, created_at, updated_at)
                            VALUES (:username, :email, :phone_number, :photo_link, :password_hash, :created_at, :updated_at)',
            [
                ':username' => $this->username,
                ':email' => $this->email,
                ':phone_number' => $this->phoneNumber,
                ':photo_link' => $this->photoLink,
                ':password_hash' => $this->passwordHash,
                ':created_at' => date('Y-m-d H:i:s'),
                ':updated_at' => date('Y-m-d H:i:s'),
            ]
        );
    }

    public function update(): void
    {
        Database::executeQuery(
            'UPDATE user SET username = :username, email = :email, phone_number = :phone_number, photo_link = :photo_link, updated_at = :updated_at
                        WHERE id = :id',
            [
                ':username' => $this->username,
                ':email' => $this->email,
                ':phone_number' => $this->phoneNumber,
                ':photo_link' => $this->photoLink,
                ':updated_at' => date('Y-m-d H:i:s'),
                ':id' => $this->id,
            ]
        );
    }

    /**
     * @return User[]|null
     */
    public static function find(string $selectQuery = 'SELECT * FROM user', array $params = []): ?array
    {
        $usersAsArray = Database::executeQuery($selectQuery, $params);
        if ([] === $usersAsArray) {
            return null;
        }

        $users = [];
        foreach ($usersAsArray as $userFields) {
            $user = new User($userFields['username'], $userFields['email'], $userFields['phone_number'], $userFields['photo_link'], $userFields['password_hash']);
            $user->id = (int)$userFields['id'];
            $user->createdAt = new DateTimeImmutable($userFields['created_at']);
            $user->updatedAt = new DateTimeImmutable($userFields['updated_at']);

            $users[] = $user;
        }

        return $users;
    }

    public static function findOne(string $selectQuery = 'SELECT * FROM user LIMIT 1', array $params = []): ?self
    {
        $userAsArray = Database::executeQuery($selectQuery, $params);
        if ([] === $userAsArray) {
            return null;
        }

        $user = new User(
            $userAsArray[0]['username'],
            $userAsArray[0]['email'],
            $userAsArray[0]['phone_number'],
            $userAsArray[0]['photo_link'],
            $userAsArray[0]['password_hash']
        );
        $user->id = (int)$userAsArray[0]['id'];
        $user->createdAt = new DateTimeImmutable($userAsArray[0]['created_at']);
        $user->updatedAt = new DateTimeImmutable($userAsArray[0]['updated_at']);


        return $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getPhotoLink(): string
    {
        return $this->photoLink;
    }

    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
