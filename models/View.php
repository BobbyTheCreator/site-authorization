<?php

final class View
{
    public array $params = [];

    public function __construct($params = [])
    {
        $this->params = $params;
    }
}
