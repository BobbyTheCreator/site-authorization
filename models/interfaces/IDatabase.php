<?php

declare(strict_types=1);

interface IDatabase
{
    public function insert(): void;
    public function update(): void;

    /**
     * @return self[]|null
     */
    public static function find(string $selectQuery = '', array $params = []): ?array;
    public static function findOne(string $selectQuery = '', array $params = []): ?self;
}
