<?php

class Route
{
    public static array $validRoutes = [];

    public static function set($route, $function): void
    {
        self::$validRoutes[] = $route;

        if ($_GET['url'] === $route) {
            $function->__invoke();
        }
    }
}
