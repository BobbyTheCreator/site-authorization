// formValidation is used in views/register
formValidation = (function ($) {
    let config = {
        usernameValidation: '.js-username-validation',
        emailValidation: '.js-email-validation',
        phoneNumberValidation: '.js-phone-number-validation',
        fileValidation: '.js-file-validation',
        passwordValidation: '.js-password-validation',
        confirmPasswordValidation: '.js-confirm-password-validation',
    };

    let init = function (settings) {
        $.extend(config, settings || {});
        setup();
    };

    let setup = function () {
        $(document).on('focusout', config.usernameValidation, usernameValidation);
        $(document).on('focusout', config.emailValidation, emailValidation);
        $(document).on('focusout', config.phoneNumberValidation, phoneNumberValidation);
        $(document).on('focusout', config.fileValidation, fileValidation);
        $(document).on('focusout', config.passwordValidation, passwordValidation);
        $(document).on('focusout', config.confirmPasswordValidation, confirmPasswordValidation);
    };

    let usernameValidation = function () {
        let username = $.trim($(this).val());
        if ('' === username) {
            setErrorFor($(this), config.errorTranslations['Username cannot be empty']);
        } else {
            setSuccessFor($(this));
        }
    };

    let emailValidation = function () {
        let email = $.trim($(this).val());
        if ('' === email) {
            setErrorFor($(this), config.errorTranslations['Email cannot be empty']);
        } else if (!isEmail(email)) {
            setErrorFor($(this), config.errorTranslations['Invalid email']);
        } else {
            setSuccessFor($(this));
        }
    };

    let phoneNumberValidation = function () {
        let phoneNumber = $.trim($(this).val());
        if ('' === phoneNumber) {
            setErrorFor($(this), config.errorTranslations['Phone number cannot be empty']);
        } else if (phoneNumber.length !== 10) {
            setErrorFor($(this), config.errorTranslations['Wrong number of digits']);
        } else if (null === phoneNumber.match(/^\d+$/)) {
            setErrorFor($(this), config.errorTranslations['Phone number has non digits']);
        } else {
            setSuccessFor($(this));
        }
    };

    let fileValidation = function () {
        let file = $.trim($(this).val());
        if ('' === file) {
            setErrorFor($(this), config.errorTranslations['File cannot be empty']);
        } else if (!isValidFileFormat(file)) {
            setErrorFor($(this), config.errorTranslations['Invalid file format']);
        } else {
            setSuccessFor($(this));
        }
    };

    let passwordValidation = function () {
        let password = $.trim($(this).val());
        if ('' === password) {
            setErrorFor($(this), config.errorTranslations['Password cannot be empty']);
        } else {
            setSuccessFor($(this));
        }
    };

    let confirmPasswordValidation = function () {
        let confirmPassword = $.trim($(this).val());
        if ('' === confirmPassword) {
            setErrorFor($(this), config.errorTranslations['Confirmation password cannot be empty']);
        } else if ($(config.passwordValidation).val() !== confirmPassword) {
            setErrorFor($(this), config.errorTranslations['Passwords do not match']);
        } else {
            setSuccessFor($(this));
        }
    };

    function setErrorFor(object, message) {
        object.removeClass('success');
        object.addClass('error');
        object.siblings('small').html(message);
    }

    function setSuccessFor(object) {
        object.removeClass('error');
        object.siblings('small').html('');
        object.addClass('success');
    }

    function isEmail(email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

    function isValidFileFormat(file) {
        let fileFormat = file.substring(file.lastIndexOf('.') + 1).toLowerCase();

        if ('gif' === fileFormat || 'jpg' === fileFormat || 'png' === fileFormat) {
            return true;
        }

        return false;
    }

    return {
        init: init,
    };

})(jQuery);