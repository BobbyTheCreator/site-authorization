<?php

require_once '../models/Helper.php';
require_once '../models/User.php';
require_once '../models/SignInError.php';

session_start();

$codeErrorString = '';
$email = $password = '';
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    if (empty($_POST['inputEmail'])) {
        $codeErrorString .= SignInError::EMAIL_IS_EMPTY;
    } else {
        $email = Helper::testInput($_POST['inputEmail']);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $codeErrorString .= SignInError::INVALID_EMAIL;
        }
    }

    if (empty($_POST['inputPassword'])) {
        $codeErrorString .= SignInError::PASSWORD_IS_EMPTY;
    } else {
        $password = Helper::testInput($_POST['inputPassword']);
    }

    if ('' === $codeErrorString) {
        if (null === $user = User::findOne('SELECT * FROM user WHERE email = :email', [':email' => $email])) {
            $codeErrorString .= SignInError::EMAIL_OR_PASSWORD_DO_NOT_MATCH;
        } elseif (false === password_verify($password, $user->getPasswordHash())) {
            $codeErrorString .= SignInError::EMAIL_OR_PASSWORD_DO_NOT_MATCH;
        }
    }

    if ('' !== $codeErrorString) {
        $_SESSION['formFields']['inputEmail'] = $email;

        header('Location: '.'/login/?err='.$codeErrorString);
        exit();
    }
    unset($_SESSION['formFields']);

    $_SESSION['authorizedUser'] = $email;

    header('Location: '.'/profile');
}
