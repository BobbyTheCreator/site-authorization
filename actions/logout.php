<?php

session_start();

unset($_SESSION['authorizedUser']);

header('Location: ' . $_SERVER['HTTP_REFERER']);
exit();
