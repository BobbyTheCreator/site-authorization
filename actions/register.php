<?php

require_once '../models/Helper.php';
require_once '../models/User.php';
require_once '../models/SignUpError.php';

const MAX_FILE_UPLOAD_SIZE = 4194304; // 4MB in bytes

session_start();

$codeErrorString = '';
$userName = $email = $fileAsString = $password = '';
$phoneNumber = 0;
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    if (empty($_POST['inputUsername'])) {
        $codeErrorString .= SignUpError::USERNAME_IS_EMPTY;
    } else {
        $userName = Helper::testInput($_POST['inputUsername']);
    }

    if (empty($_POST['inputEmail'])) {
        $codeErrorString .= SignUpError::EMAIL_IS_EMPTY;
    } else {
        $email = Helper::testInput($_POST['inputEmail']);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $codeErrorString .= SignUpError::INVALID_EMAIL;
        }
        if (null !== User::findOne('SELECT * FROM user WHERE email = :email', [':email' => $email])) {
            $codeErrorString .= SignUpError::EMAIL_ALREADY_EXISTS;
        }
    }

    if (empty($_POST['inputPhoneNumber'])) {
        $codeErrorString .= SignUpError::PHONE_NUMBER_IS_EMPTY;
    } else {
        $phoneNumber = Helper::testInput($_POST['inputPhoneNumber']);
        if (strlen($phoneNumber) !== 10) {
            $codeErrorString .= SignUpError::WRONG_NUMBER_OF_DIGITS_FOR_PHONE_NUMBER;
        } elseif (!is_numeric($phoneNumber)) {
            $codeErrorString .= SignUpError::PHONE_NUMBER_HAS_NON_DIGITS;
        } elseif (null !== User::findOne('SELECT * FROM user WHERE phone_number = :phone_number', [':phone_number' => '+7'.$phoneNumber])) {
            $codeErrorString .= SignUpError::PHONE_NUMBER_ALREADY_EXISTS;
        }
    }

    if (empty($_FILES['inputFile']['name'])) {
        $codeErrorString .= SignUpError::FILE_IS_EMPTY;
    } else {
        $fileExtension = explode('/', $_FILES['inputFile']['type'])[1];
        if ('gif' !== $fileExtension && 'jpg' !== $fileExtension && 'png' !== $fileExtension) {
            $codeErrorString .= SignUpError::INVALID_FILE_FORMAT;
        } elseif (MAX_FILE_UPLOAD_SIZE < $_FILES['inputFile']['size']) {
            $codeErrorString .= SignUpError::UPLOAD_FILE_TOO_LARGE;
        }
    }

    if (empty($_POST['inputPassword'])) {
        $codeErrorString .= SignUpError::PASSWORD_IS_EMPTY;
    } else {
        $password = Helper::testInput($_POST['inputPassword']);
    }

    if (empty($_POST['inputConfirmPassword'])) {
        $codeErrorString .= SignUpError::CONFIRM_PASSWORD_IS_EMPTY;
    } else {
        $confirmPassword = Helper::testInput($_POST['inputConfirmPassword']);
        if ($confirmPassword !== $password) {
            $codeErrorString .= SignUpError::PASSWORDS_DO_NOT_MATCH;
        }
    }

    if ('' !== $codeErrorString) {
        $_SESSION['formFields']['inputUsername'] = $userName;
        $_SESSION['formFields']['inputEmail'] = $email;
        $_SESSION['formFields']['inputPhoneNumber'] = $phoneNumber;

        header('Location: '.'/register/?err='.$codeErrorString);
        exit();
    }
    unset($_SESSION['formFields']);

    $path = pathinfo($_FILES['inputFile']['name']);
    $tempLocation = $_FILES['inputFile']['tmp_name'];
    $newFilePath = '/web/uploads/profile-photos/'.$email.$path['filename'].'.'.$path['extension'];
    if (false === move_uploaded_file($tempLocation, '..'.$newFilePath)) {
        exit('Cannot upload file for some reason. Please contact support: temochkaykvlv@gmail.com. Have a nice day!');
    }

    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
    $user = new User($userName, $email, '+7'.$phoneNumber, $newFilePath, $passwordHash);
    $user->insert();

    $_SESSION['authorizedUser'] = $email;

    header('Location: '.'/profile');
}
