<?php

/* @var array $lang */

Route::set('index', function () {
    Index::CreateView('index');
});

Route::set('about-us', function () {
    AboutUs::CreateView('about-us');
});

Route::set('contact-us', function () {
    ContactUs::CreateView('contact-us');
});

Route::set('login', function () {
    Login::CreateView('login');
});

Route::set('register', function () {
    Register::CreateView('register');
});

Route::set('profile', function () {
    Profile::CreateView('profile');
});
